package com.company;

public class Main {

    public static void main(String[] args) {
        // Instanciar cada clase
        Object obj = new Object();
        Fruit fruta = new Fruit();
        Apple apple=new Apple();
        Citrus citrus =new Citrus();
        Orange orange= new Orange();
        Sqeezable sq;

        //Cast de Fruit a Apple y casting de Apple a Fruit
        try{
            Apple apple1= (Apple)fruta;
            System.out.println("Verificacion de cast para Fruit-Apple: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        try{
            Fruit fruta1= (Fruit)apple;
            System.out.println("Verificacion de cast para Apple-Fruit: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        //Cast de Fruit a Sqezable
        try{
            Sqeezable sq1= (Sqeezable) fruta;
            System.out.println("Verificacion de cast para Fruit-Sqeezable: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        //Cast de Fruit a Citrus y casting de Citrus a Fruit
        try{
            Citrus citrus1= (Citrus) fruta;
            System.out.println("Verificacion de cast para Fruit-Citrus: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        try{
            Fruit fruta1= (Fruit)citrus;
            System.out.println("Verificacion de cast para Citrus-Fruit: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        //Cast de Fruit a Orange y casting de Orange a Fruit
        try{
            Orange orange1= (Orange) fruta;
            System.out.println("Verificacion de cast para Fruit-Orange: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        try{
            Fruit fruta1= (Fruit)orange;
            System.out.println("Verificacion de cast para Orange-Fruit: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        //casting de Citrus a Orange
        try{
            Orange orange1 =(Orange) citrus;
            System.out.println("Verificacion de cast para Citrus-Orange: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        //casting de Apple a Sqeezable
        try{
            Sqeezable sq1= (Sqeezable) apple;
            System.out.println("Verificacion de cast para Apple-Sqeezable: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        //casting de Citrus a Sqeezable
        try{
            Sqeezable sq1 =(Sqeezable) citrus;
            System.out.println("Verificacion de cast para Citrus-Sqeezable: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        //casting de Orange a Sqeezable
        try{
            Sqeezable sq1 =(Sqeezable) orange;
            System.out.println("Verificacion de cast para Orange-Sqeezable: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }


        //casting de Orange a Citrus y casting Citrus a Orange
        try{
            Citrus citrus1=(Citrus) orange;
            System.out.println("Verificacion de cast para Orange-Citrus: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

        try{
            Orange orange1 =(Orange)citrus;
            System.out.println("Verificacion de cast para Orange-Citrus: cast listo ");
        }catch (ClassCastException e){
            System.out.println("cast no listo: ");
        }

    }
}
